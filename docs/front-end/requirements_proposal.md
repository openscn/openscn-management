I made some notes on the front end.
More information can be found here:
[ google drive link ]( https://docs.google.com/document/d/18lW8l-9QXyr0jbISylBlHKvqnkuYmiKFDCnlCApAOWA/edit?usp=sharing)

I will transform it to markdown and save it here when its maturity level is
appropriate.

The repo is public (so this file), so I gave comment permissions. Edit
permissions will be granted by email to the smart campus members.
