### Meeting notes 2019-04-03

Valantis joined our team. We presented our work so far.

Some work was done on the web-API.

tasks for next meeting:
  - Update documentation for the infrastructure (dpliakos).
  - Add the web-api at the dcker-compose bundle (dpliakos).
  - Add Make the docker-composer-bundle development friendly (dpliakos).
  - Review JSON handling at the mc_framework. (Q, Valantis)
  - Add HTTP support (instread of just HTTPS) to the mc_framework. (Q, Valantis)


Research tasks (long term):
  - check out micropython.

Practical:
  - Find a meeting point with enough space and available power plugs.
